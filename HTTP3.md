
# HTTP/3 là gì, và tại sao nó lại quan trọng

<h2> HTTP/3, HTTP thông qua QUIC (Quick Connections UDP Internet) đều mang đến những hiệu xuất và tính năng quan trọng </h2>

Khi nghiên cứu về internet và các công nghệ đằng sau nó, bạn có thể đã bắt gặp thuật ngữ này: HTTP. HTTP, hay Hypertext Transfer Protocol, là xương sống của web và là giao thức phổ biến để truyền dữ liệu văn bản. Bạn chắc chắn đã sử dụng nó, vì trang web bạn đã học về HTTP trên sử dụng HTTP.

<h2> Giới thiệu </h2>

<h3>Lịch sử của HTTP </h3>
Phiên bản đầu tiên của HTTP được phát hành là HTTP / 0.9. Tim Berners-Lee đã tạo ra nó vào năm 1989, và nó được đặt tên là HTTP / 0.9 vào năm 1991. HTTP / 0.9 bị giới hạn và chỉ có thể làm những việc cơ bản. Nó không thể trả về bất kỳ thứ gì khác ngoài một trang web và không hỗ trợ cookie và các tính năng hiện đại khác. Năm 1996, HTTP / 1.0 được phát hành, mang đến các tính năng mới như yêu cầu POST và khả năng gửi thứ gì đó khác ngoài trang web. Tuy nhiên, nó vẫn còn một chặng đường dài so với ngày nay. HTTP / 1.1 được phát hành vào năm 1997 và đã được sửa đổi hai lần, một lần vào năm 1999 và một lần vào năm 2007. Nó mang lại nhiều tính năng mới chính như cookie và kết nối vẫn tồn tại. Cuối cùng, vào năm 2015, HTTP / 2 đã được phát hành và cho phép tăng hiệu suất, làm cho những thứ như Sự kiện được gửi từ máy chủ và khả năng gửi nhiều yêu cầu cùng một lúc. HTTP / 2 vẫn còn mới và chỉ được sử dụng bởi ít hơn một nửa số [trang web](https://w3techs.com/technologies/details/ce-http2).

<h2> HTTP/3: Phiên bản mới nhất của HTTP </h2>

HTTP / 3, hoặc HTTP qua QUIC, thay đổi HTTP rất nhiều. HTTP theo truyền thống được thực hiện trên TCP, Giao thức điều khiển truyền. Tuy nhiên, TCP được phát triển vào năm 1974, vào thời kỳ đầu của internet. Khi TCP ban đầu được tạo ra, các tác giả của nó không thể dự đoán được sự phát triển của web. Do TCP đã lỗi thời nên TCP đã hạn chế HTTP trong một thời gian cả về tốc độ và bảo mật. Bây giờ, vì HTTP / 3, HTTP không bị giới hạn nữa. Thay vì TCP, HTTP / 3 sử dụng một giao thức mới do Google phát triển vào năm 2012, được gọi là QUIC (phát âm là “quick”). Điều này giới thiệu nhiều tính năng mới cho HTTP.

<h2> Đặc trưng </h2>
<h3>request multiplexing nhanh</h3>
Trước HTTP/2, các trình duyệt chỉ có thể gửi một request đến server tại một thời điểm. Điều này khiến trang web tải chậm hơn đáng kể vì trình duyệt chỉ tải một nội dung, như CSS hoặc JavaScript tại một thời điểm. HTTP / 2 đã giới thiệu khả năng tải nhiều nội dung cùng một lúc, nhưng TCP không được tạo ra cho việc này. Nếu một trong các request không thành công, TCP sẽ yêu cầu trình duyệt thực hiện lại tất cả các request . Vì TCP đã bị loại bỏ trong HTTP / 3 và được thay thế bằng QUIC, HTTP / 3 đã giải quyết được vấn đề này. Với HTTP / 3, trình duyệt chỉ cần thực hiện lại request không thành công. Do đó, HTTP / 3 nhanh hơn và đáng tin cậy hơn.

<h3> Mã hóa nhanh </h3>
HTTP / 3 tối ưu hóa "handshake" cho phép các HTTP request của trình duyệt được mã hóa. QUIC kết hợp kết nối ban đầu với kết nối TLS, làm cho nó an toàn theo mặc định và nhanh hơn.

<h2> Thực thi </h2>
<h3> Tiêu chuẩn hóa </h3>
Tại thời điểm viết bài này, HTTP / 3 và QUIC chưa được chuẩn hóa. Có một [Nhóm công tác](https://quicwg.org/) IETF hiện đang làm việc trên một bản dự thảo để tiêu chuẩn hóa QUIC. Phiên bản QUIC cho HTTP / 3 được sửa đổi một chút, sử dụng TLS thay vì mã hóa của Google, nhưng nó có cùng ưu điểm.

<h3> Hỗ trợ trình duyệt</h3>

Hiện tại, Chrome hỗ trợ HTTP / 3 theo mặc định do Google tạo giao thức QUIC và đề xuất HTTP qua QUIC. Firefox cũng hỗ trợ giao thức trong các phiên bản 88+ mà không có cờ. Safari 14 hỗ trợ HTTP / 3, nhưng chỉ khi cờ tính năng thử nghiệm được bật.

<h3>  Hỗ trợ Serverless / CDN </h3>

Cho đến nay, chỉ một số server hỗ trợ HTTP / 3, nhưng thị phần của chúng đang tăng lên. Cloudflare là một trong những công ty đầu tiên ngoài Google hỗ trợ HTTP / 3, vì vậy các chức năng không máy chủ và CDN của họ đều tuân thủ HTTP / 3. Ngoài ra, Google Cloud và Fastly tuân thủ HTTP / 3. Thật không may, Microsoft Azure CDN và AWS CloudFront dường như không hỗ trợ HTTP / 3 ở thời điểm hiện tại. Nếu bạn muốn dùng thử HTTP / 3, [QUIC.Cloud](https://quic.cloud/) là một cách thú vị (thử nghiệm) để thiết lập một CDN HTTP / 3 trong bộ nhớ đệm trước server của bạn. Cloudflare, Fastly và Google Cloud cũng có hỗ trợ HTTP / 3 tốt và sẵn sàng sản xuất hơn.

<h2> Phần kết luận </h2>

HTTP / 3 vẫn là một bản cập nhật thử nghiệm cho HTTP và nó có thể sẽ thay đổi. Tuy nhiên, hơn một nửa số người dùng ủng hộ hình thức HTTP / 3 hiện tại. Nếu bạn chuẩn bị cập nhật triển khai của mình, thì đó có thể là một bước tăng hiệu suất đáng hoan nghênh. Tôi hy vọng bạn thích đọc và học được điều gì đó từ bài viết này.
